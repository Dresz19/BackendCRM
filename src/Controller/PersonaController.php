<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PersonaController extends AbstractController
{
    /**
     *  @Route("/persona/eliminar", name="eliminar_persona")
     * 
     */
    public function eliminarPersona(Request $request){
        $id = $request->get('nombre');
        $response = new JsonResponse();
        $response -> setData([
             'success' => true,
             //'mensaje' => 'Eliminado correctamente'
             'mensaje' => 'Hola '. $id
        ]);
        return $response;
    }
    /**
     *  @Route("/persona/agregar", name="agregar_persona")
     * 
     */
    public function agregarPersona(Request $request){
        $nombres = $request->get('nombres');
        $apellidos = $request->get('apellidos');
        $direccion = $request->get('direccion');
        $padron = $request->get('padron');
        $telefono = $request->get('telefono');
        $zona = $request->get('zona');
        $fecha = $request->get('fecha_nacimiento');
        $cui = $request->get('cui');
        
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Agregado correctamente'
        ]);
        return $response;
    }
    /**
     *  @Route("/persona/modificar", name="modificar_persona")
     * 
     */
    public function modificarPersona(Request $request){
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Modificado correctamente'
        ]);
        return $response;
    }
}
