<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Sede;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SedeController extends AbstractController
{
    /**
     *  @Route("/sede/eliminar", name="eliminar_sede")
     * 
     */
    public function eliminarSede(Request $request){
        $id = $request->get('id');
        $entityManager = $this->getDoctrine()->getManager();
        $sede = $entityManager->getRepository(Sede::class)->find($id);
        $response = new JsonResponse();

        if (!$sede) {
            
            $response -> setData([
                 'success' => 0,
                 'mensaje' => 'No se encontro la sede'
            ]);
            return $response;
        }
        
        $entityManager->remove($sede);
        $entityManager->flush();
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Eliminado correctamente'
        ]);
        return $response;
    }
    /**
     *  @Route("/sede/agregar", name="agregar_sede")
     * 
     */
    public function agregarSede(Request $request){
        $nombre = $request->get('nombre');
        $direccion = $request->get('direccion');
        $municipio = $request->get('municipio_id');
        $entityManager = $this->getDoctrine()->getManager();
        $sede = new Sede();
        $sede->setNombre($nombre);
        $sede->setDireccion($direccion);
        $sede->setMunicipioId($municipio);
        
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($sede);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        $response = new JsonResponse();
        
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Agregado correctamente'
        ]);
        return $response;
    }
    /**
     *  @Route("/sede/modificar", name="modificar_sede")
     * 
     */
    public function modificarSede(Request $request){
        $id = $request->get('id');
        $nombre = $request->get('nombre');
        $direccion = $request->get('direccion');
        $municipio = $request->get('municipio_id');

        $entityManager = $this->getDoctrine()->getManager();
        $sede = $entityManager->getRepository(Sede::class)->find($id);
        $response = new JsonResponse();

        if (!$sede) {
            
            $response -> setData([
                 'success' => 0,
                 'mensaje' => 'No se encontro la sede'
            ]);
            return $response;
        }

        $sede->setNombre($nombre);
        $sede->setDireccion($direccion);
        $sede->setMunicipioId($municipio);
        $entityManager->flush();    

        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Modificado correctamente'
        ]);
        return $response;
    }

    /**
     * @Route("/sede/ver", name="ver_sede")
     */
    public function verSede(Request $request)
    {
        $id = $request->get('id');
        $sede = $this->getDoctrine()
            ->getRepository(Sede::class)
            ->find($id);
        $response = new JsonResponse();
        if (!$sede) {
            
            $response -> setData([
                 'success' => 0,
                 'mensaje' => 'No se encontro la sede'
            ]);
            return $response;
        }
        $response -> setData([
             'success' => 1,
             'id' => $sede->getId(),
             'Nombre'=>$sede->getNombre(),
             'Direccion'=>$sede->getDireccion(),
             'Municipio'=>$sede->getMunicipioId()
        ]);
        return $response;

        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }
}