<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EtiquetaController extends AbstractController
{
    /**
     *  @Route("/etiqueta/eliminar", name="eliminar_etiqueta")
     * 
     */
    public function eliminarEtiqueta(Request $request){
        $id = $request->get('nombre');
        $response = new JsonResponse();
        $response -> setData([
             'success' => true,
             //'mensaje' => 'Eliminado correctamente'
             'mensaje' => 'Hola '. $id
        ]);
        return $response;
    }
    /**
     *  @Route("/etiqueta/agregar", name="agregar_etiqueta")
     * 
     */
    public function agregarEtiqueta(Request $request){
        $nombre = $request->get('nombre');
        $clasificacion = $request->get('clasificacion_id');
        
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Agregado correctamente'
        ]);
        return $response;
    }
    /**
     *  @Route("/etiqueta/modificar", name="modificar_etiqueta")
     * 
     */
    public function modificarEtiqueta(Request $request){
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Modificado correctamente'
        ]);
        return $response;
    }
}