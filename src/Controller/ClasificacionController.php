<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClasificacionController extends AbstractController
{
    /**
     *  @Route("/clasificacion/eliminar", name="eliminar_clasificacion")
     * 
     */
    public function eliminarClasificacion(Request $request){
        $id = $request->get('nombre');
        $response = new JsonResponse();
        $response -> setData([
             'success' => true,
             //'mensaje' => 'Eliminado correctamente'
             'mensaje' => 'Hola '. $id
        ]);
        return $response;
    }
    /**
     *  @Route("/clasificacion/agregar", name="agregar_clasificacion")
     * 
     */
    public function agregarClasificacion(Request $request){
        $nombre = $request->get('nombre');
        $tipo = $request->get('tipo');
        
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Agregado correctamente'
        ]);
        return $response;
    }
    /**
     *  @Route("/clasificacion/modificar", name="modificar_clasificacion")
     * 
     */
    public function modificarClasificaicon(Request $request){
        $response = new JsonResponse();
        $response -> setData([
             'success' => 1,
             'mensaje' => 'Modificado correctamente'
        ]);
        return $response;
    }
}